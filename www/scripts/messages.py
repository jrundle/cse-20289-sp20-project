#!/usr/bin/env python3

import cgi
import datetime
import time
import os

print('HTTP/1.0 200 OK')
print('Content-Type: text/html')
print()

form = cgi.FieldStorage()
messages = "data/messages.csv"


if not os.path.exists(messages):
    os.makedirs(os.path.dirname(messages), exist_ok=True)
    open(messages, 'w').close()



if 'message' in form:
    with open(messages, 'a+') as f:
        name    = form['name'].value
        message = form['message'].value
        now     = time.time()

        if name != "" and message != "":
            f.write(f"{name},{message},{now}\n")


print('''
<html>
    <head>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    
    <body>
        <div class="container">
            <a class="btn" href="/scripts">Go Back</a>
            <h1 class="text-center">Messages Board</h1>
            
            <table class="table text-center">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Message</th>
                  <th scope="col">Timestamp</th>
                </tr>
              </thead>
              <tbody>
''')

file = open(messages, 'r')

for line in file.readlines():
    name, message, time = line.split(',')
    time = datetime.datetime.fromtimestamp(float(time)).strftime("%c")
    print(f"<tr><td>{name}</td><td>{message}</td><td>{time}</td></tr>")

file.close()

print(f'''
            </table>
            
            <form>
              <div class="row">
                <div class="col">
                  <input type="text" class="form-control" name="name" placeholder="Name">
                </div>
                <div class="col">
                  <input type="text" class="form-control" name="message" placeholder="Message">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>              
              </div>
            </form>
        </div>
    </body>
</html>
''')
