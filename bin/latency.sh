#!/bin/sh


CLIENT=thor.py
SERVER=${1:-localhost}
PORT=${2:-9898}

tmp=.total
touch $tmp

if ! nc -z $SERVER $PORT; then
    echo "Unable to connect"
    exit 1;
fi


SERVER="http://$SERVER:$PORT"


check_endpoints() {
    for i in `seq 2 8`; do
        echo "" > $tmp

        for uri in $@; do
            "./$CLIENT" -h $i -t 10 "$SERVER$uri" | grep 'TIME' | awk '{printf $5}' >> $tmp
        done

        printf "$i\t"
        awk '{s+=$1} END {printf s/NR}' $tmp
        echo ""
    done
}

echo "Checking latency"
echo "----------------"
echo ""

echo "Directory Listings"
URIS="/ /html /images /scripts /text"
check_endpoints $URIS
echo ""

echo "Static Files"
URIS="/song.txt /text/hackers.txt /text/lyrics.txt /text/pass/fail/html /index.html"
check_endpoints $URIS
echo ""

echo "Images"
URIS="/images/a.png /images/b.jpg /images/c.jpg /images/d.png"
check_endpoints $URIS
echo ""

echo "Scripts"
URIS="/scripts/cowsay.sh /scripts/env.sh /scripts/hello.py"
check_endpoints $URIS

rm $tmp

