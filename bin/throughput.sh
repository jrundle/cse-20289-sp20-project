#!/bin/sh

CLIENT=thor.py
SERVER=${1:-localhost}
PORT=${2:-9898}

if ! nc -z $SERVER $PORT; then
    echo "Unable to connect"
    exit 1;
fi

SERVER="http://$SERVER:$PORT"

small="/static/1K"
medium="/static/1M"
large="/static/1G"

./$CLIENT -t 5 "$SERVER/" > /dev/null  #dummy request

for i in `seq 2`; do
    echo "Retrieving Small File"
    ./$CLIENT -h $i -t 15 "$SERVER$small" | grep 'TIME' | awk '{printf "%.2f %s\n",  1/(1024*$5), "MBps"}'

    echo "Retrieving Medium File"
    ./$CLIENT -h $i  -t 15 "$SERVER$medium" | grep 'TIME' | awk '{printf "%.2f %s\n", 1/($5), "MBps"}'

    echo "Retrieving Large File"
    ./$CLIENT -h $i -t 10 "$SERVER$large" | grep 'TIME' | awk '{printf "%.2f %s\n", 1024/$5, "MBps"}'
done

