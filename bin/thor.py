#!/usr/bin/env python3

import concurrent.futures
import os
import requests
import sys
import time

# Functions

def usage(status=0):
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [-h HAMMERS -t THROWS] URL
    -h  HAMMERS     Number of hammers to utilize (1)
    -t  THROWS      Number of throws per hammer  (1)
    -v              Display verbose output
    ''')
    sys.exit(status)


def hammer(url, throws, verbose, hid):
    ''' Hammer specified url by making multiple throws (ie. HTTP requests).

    - url:      URL to request
    - throws:   How many times to make the request
    - verbose:  Whether or not to display the text of the response
    - hid:      Unique hammer identifier

    Return the average elapsed time of all the throws.
    '''
    times = []

    for throw in range(throws):
        start = time.time()

        resp = requests.get(url)

        if verbose:
            print(resp.text)

        elapsed = time.time() - start
        times.append(elapsed)

        print(f"Hammer: {hid}, Throw:   {throw}, Elapsed Time: {elapsed:.2f}")

    avg = sum(times) / throws
    print(f"Hammer: {hid}, AVERAGE   , Elapsed Time: {avg:.2f}")

    return avg


def do_hammer(args):
    ''' Use args tuple to call `hammer` '''
    return hammer(*args)


def main():
    hammers = 1
    throws  = 1
    verbose = False

    # Parse command line arguments
    argind = 1
    while argind < len(sys.argv) and sys.argv[argind].startswith("-"):
        arg = sys.argv[argind][1:]

        if arg in ('h', 'f') and argind + 1 >= len(sys.argv):
            usage(1)

        if arg == 'h':
            hammers = int(sys.argv[argind + 1])
            argind += 1

        elif arg == 't':
            throws = int(sys.argv[argind + 1])
            argind += 1

        elif arg == 'v':
            verbose = True

        else:
            usage(1)

        argind += 1

    if argind + 1 != len(sys.argv):
        usage(1)

    url = sys.argv[argind]


    # Create pool of workers and perform throws
    args = (
        (url, throws, verbose, hid) for hid in range(hammers)
    )

    with concurrent.futures.ProcessPoolExecutor(max_workers=hammers) as executor:
        times = executor.map(do_hammer, args)

    avg = sum(times) / hammers
    print(f"TOTAL AVERAGE ELAPSED TIME: {avg:.2f}")

# Main execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
