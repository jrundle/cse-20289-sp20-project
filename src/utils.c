/* utils.c: spidey utilities */

#include "spidey.h"

#include <ctype.h>
#include <errno.h>
#include <string.h>

#include <sys/stat.h>
#include <unistd.h>

/**
 * Determine mime-type from file extension.
 *
 * @param   path        Path to file.
 * @return  An allocated string containing the mime-type of the specified file.
 *
 * This function first finds the file's extension and then scans the contents
 * of the MimeTypesPath file to determine which mimetype the file has.
 *
 * The MimeTypesPath file (typically /etc/mime.types) consists of rules in the
 * following format:
 *
 *  <MIMETYPE>      <EXT1> <EXT2> ...
 *
 * This function simply checks the file extension version each extension for
 * each mimetype and returns the mimetype on the first match.
 *
 * If no extension exists or no matching mimetype is found, then return
 * DefaultMimeType.
 *
 * This function returns an allocated string that must be free'd.
 **/
char * determine_mimetype(const char *path) {
    char *ext;
    char *mimetype;
    char *token;
    char buffer[BUFSIZ];
    FILE *fs = NULL;

    /* Find file extension */
    ext = strrchr(path, '.');
    if (!ext) {
        debug("Unable to determine file name.  Using DefaultMimeType.");
        return strdup(DefaultMimeType);
    }
    ext++;   // exclude '.' prefix

    /* Open MimeTypesPath file */
    fs = fopen(MimeTypesPath, "r");
    if (!fs) {
        debug("Unable to open MimeTypes file.  Using DefaultMimeType.");
        return strdup(DefaultMimeType);
    }

    /* Scan file for matching file extensions */
    while (fgets(buffer, BUFSIZ, fs)) {
        // skip comments and empty lines
        if (buffer[0] == '#' || buffer[0] == '\n')
            continue;

        // <mimetype>    <token> <token>\n
        mimetype = strtok(buffer, WHITESPACE);

        // check each extension
        while ((token = strtok(NULL, WHITESPACE))) {
            skip_whitespace(token);

            if (streq(token, ext)) {
                // match found
                fclose(fs);
                return strdup(mimetype);
            }
        }
    }

    debug("No MimeType match found for %s.  Using DefaultMimeType.", path);
    fclose(fs);
    return strdup(DefaultMimeType);
}

/**
 * Determine actual filesystem path based on RootPath and URI.
 *
 * @param   uri         Resource path of URI.
 * @return  An allocated string containing the full path of the resource on the
 * local filesystem.
 *
 * This function uses realpath(3) to generate the realpath of the
 * file requested in the URI.
 *
 * As a security check, if the real path does not begin with the RootPath, then
 * return NULL.
 *
 * Otherwise, return a newly allocated string containing the real path.  This
 * string must later be free'd.
 **/
char * determine_request_path(const char *uri) {
    char path[BUFSIZ];
    strcpy(path, RootPath);
    strcat(path, uri);

    char *fullpath = realpath(path, NULL);
    if (!fullpath) {
        return NULL;
    }

    // make sure fullpath begins with rootpath
    if (strncmp(RootPath, fullpath, strlen(RootPath)) != 0) {
        free(fullpath);
        return NULL;
    }

    return fullpath;
}

/**
 * Return static string corresponding to HTTP Status code.
 *
 * @param   status      HTTP Status.
 * @return  Corresponding HTTP Status string (or NULL if not present).
 *
 * http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 **/
const char * http_status_string(Status status) {
    static char *StatusStrings[] = {
        "200 OK",
        "400 Bad Request",
        "404 Not Found",
        "500 Internal Server Error",
        "418 I'm A Teapot",
    };

    //if (status >= sizeof(StatusStrings) / sizeof(StatusStrings[0]))
    //    return NULL;

    return StatusStrings[status];
}

/**
 * Advance string pointer pass all nonwhitespace characters
 *
 * @param   s           String.
 * @return  Point to first whitespace character in s.
 **/
char * skip_nonwhitespace(char *s) {
    while (*s && !isspace(*s)) s++;
    return s;
}

/**
 * Advance string pointer pass all whitespace characters
 *
 * @param   s           String.
 * @return  Point to first non-whitespace character in s.
 **/
char * skip_whitespace(char *s) {
    while (*s && isspace(*s)) s++;
    return s;
}

/**
 * Determines which icon to use based on an input file
 *
 * @param   s           String.
 * @return  icon uri corresponding to file type
 **/
char * determine_icon(char *s) {
    // parent directory
    if (strstr(s, ".."))
        return strdup("/icons/parent.png");

    // see if path is a directory
    struct  stat ss;
    char   *full_path = determine_request_path(s);
    if (!full_path) {
        debug("Ope.  That shouldn't have happened");
        return strdup("/icons/unknown.png");
    }
    if (stat(full_path, &ss) < 0) {
        debug("Failed to stat");
        free(full_path);
        return strdup("/icons/unknown.png");
    }
    if (S_ISDIR(ss.st_mode)) {
        free(full_path);
        return strdup("/icons/dir.png");
    }
    free(full_path);
    
    // not a directory, so check mimetype
    char *mimetype = determine_mimetype(s);
    if (!mimetype) {
        debug("Couldn't identify mimetype");
        return strdup("/icons/unknown.png");
    }

    char *icon = NULL;

    if (strncmp(mimetype, "image", 5) == 0 ) {
        // image files
        icon = strdup("/icons/image.png");

    } else if (strcmp(mimetype, "text/html") == 0) {
        // html files
        icon = strdup("/icons/html.png");

    } else if (strcmp(mimetype, "text/plain") == 0) {
        // plain text files
        icon = strdup("/icons/text.png");

    } else if (strncmp(mimetype, "application/x-", 13) == 0) {
        // interpreted scripts
        icon = strdup("/icons/script.png");
    }

    free(mimetype);

    if (!icon) {
        return strdup("icons/unknown.png");
    } else {
        return icon;
    }
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
