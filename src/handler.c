/* handler.c: HTTP Request Handlers */

#include "spidey.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

/* Internal Declarations */
Status handle_browse_request(Request *request);
Status handle_file_request(Request *request);
Status handle_cgi_request(Request *request);
Status handle_error(Request *request, Status status);

/**
 * Handle HTTP Request.
 *
 * @param   r           HTTP Request structure
 * @return  Status of the HTTP request.
 *
 * This parses a request, determines the request path, determines the request
 * type, and then dispatches to the appropriate handler type.
 *
 * On error, handle_error should be used with an appropriate HTTP status code.
 **/
Status  handle_request(Request *r) {
    Status result = HTTP_STATUS_OK;

    /* Parse request */
    if (parse_request(r) < 0) {
        debug("Error Parsing Request");
        return handle_error(r, HTTP_STATUS_BAD_REQUEST);
    }

    /* Determine request path */
    r->path = determine_request_path(r->uri);
    if (!r->path) {
        debug("Unable to find %s", r->uri);
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    debug("HTTP REQUEST PATH: %s", r->path);

    /* Dispatch to appropriate request handler type based on file type */
    struct stat ss;
    if (stat(r->path, &ss) < 0) {
        debug("Failed to stat: %s", strerror(errno));
        return handle_error(r, HTTP_STATUS_INTERNAL_SERVER_ERROR);
    }
    
    if (S_ISDIR(ss.st_mode)) {
        // directory
        debug("HTTP REQUEST TYPE: BROWSE");
        result = handle_browse_request(r);

    } else if (access(r->path, X_OK) == 0) {
        // executable file
        debug("HTTP REQUEST TYPE: CGI SCRIPT");
        result = handle_cgi_request(r);

    } else if (access(r->path, R_OK) == 0) {
        // readable file
        debug("HTTP REQUEST TYPE: FILE");
        result = handle_file_request(r);

    } else {
        // not supported
        result = HTTP_STATUS_BAD_REQUEST;
    }

    log("HTTP REQUEST STATUS: %s", http_status_string(result));

    if (result != HTTP_STATUS_OK) {
        handle_error(r, result);
    }

    return result;
}

/**
 * Handle browse request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP browse request.
 *
 * This lists the contents of a directory in HTML.
 *
 * If the path cannot be opened or scanned as a directory, then handle error
 * with HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_browse_request(Request *r) {
    struct dirent **entries;
    int n;

    /* Open a directory for reading or scanning */
    n = scandir(r->path, &entries, NULL, alphasort);
    if (n < 0) {
        debug("Failed scandir: %s", strerror(errno));
        return HTTP_STATUS_NOT_FOUND;
    }

    /* Write HTTP Header with OK Status and text/html Content-Type */
    fprintf(r->stream, "HTTP/1.0 200 OK\r\n");
    fprintf(r->stream, "Content-Type: text/html\r\n");
    fprintf(r->stream, "\r\n");
    
    // bootstrap CDN
    fprintf(r->stream, "<html>\n");
    fprintf(r->stream, "<head>\n");
    fprintf(r->stream, "\t<title>Index of %s</title>\n", r->uri);
    fprintf(r->stream, "\t<link href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" rel=\"stylesheet\">\n");
    fprintf(r->stream, "</head>\n");

    /* For each entry in directory, emit HTML list item */
    fprintf(r->stream, "<body>\n");
    fprintf(r->stream, "\t<h1 style=\"margin:15\">Index of %s</h1\n", r->uri);
    fprintf(r->stream, "\t<ul class=\"list-group list-group-flush\">\n");

    for (int i=0; i < n; i++) {
        // skip "." 
        if (streq(entries[i]->d_name, ".")) {
            free(entries[i]);
            continue;
        }

        // get resouce path for each dirent i.e. <uri>/<dirent>
        char path[BUFSIZ];
        strcpy(path, r->uri);
        if (path[strlen(path) - 1] != '/')
            strcat(path, "/");
        strcat(path, entries[i]->d_name);

        // html list entry
        fprintf(r->stream, "\t\t<li class=\"list-group-item\">");

        // display an icon
        char *icon = determine_icon(path);
        if (!icon) {
            debug("Unable to allocate entry for icon");
        } else {
            fprintf(r->stream, "<img src=\"%s\">", icon);
        }

        // include link to each uri
        fprintf(r->stream, "<a href=\"%s\" class=\"btn\">", path);

        // write "Parent Directory" instead of ".."
        char *name = (strcmp(entries[i]->d_name, "..") == 0) ? "Parent Directory" : entries[i]->d_name;
        fprintf(r->stream, "%s", name);

        // display preview of image
        //   - already did work to determine if image when determining icon
        //   - to save some trouble, just check if we are using image icon
        if (streq("/icons/image.png", icon)) {
            fprintf(r->stream, "<img src=\"%s\" style=\"margin: 0 20\" height=\"80\">", path);
        }

        // close html list entry
        fprintf(r->stream, "</a></li>\n");

        free(icon);
        free(entries[i]);
    }

    free(entries);

    // close list and rest of html
    fprintf(r->stream, "\t</ul>\n");
    fprintf(r->stream, "</body>\n");
    fprintf(r->stream, "</html>\n");

    /* Return OK */
    return HTTP_STATUS_OK;
}

/**
 * Handle file request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This opens and streams the contents of the specified file to the socket.
 *
 * If the path cannot be opened for reading, then handle error with
 * HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_file_request(Request *r) {
    FILE *fs;
    char buffer[BUFSIZ];
    char *mimetype = NULL;
    size_t nread;

    /* Open file for reading */
    fs = fopen(r->path, "r");
    if (!fs) {
        debug("Failed fopen: %s", strerror(errno));
        return HTTP_STATUS_NOT_FOUND;
    }

    /* Determine mimetype */
    mimetype = determine_mimetype(r->path);
    if (!mimetype) {
        goto fail;
    }

    /* Write HTTP Headers with OK status and determined Content-Type */
    fprintf(r->stream, "HTTP/1.0 200 OK\r\n");
    fprintf(r->stream, "Content-Type: %s\r\n", mimetype);
    fprintf(r->stream, "\r\n");

    /* Read from file and write to socket in chunks */
    while ((nread = fread(buffer, 1, BUFSIZ, fs))) {
        fwrite(buffer, 1, nread, r->stream);
    }

    /* Close file, deallocate mimetype, return OK */
    fclose(fs);
    free(mimetype);
    return HTTP_STATUS_OK;

fail:
    /* Close file, free mimetype, return INTERNAL_SERVER_ERROR */
    fclose(fs);
    free(mimetype);
    return HTTP_STATUS_INTERNAL_SERVER_ERROR;
}

/**
 * Handle CGI request
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This popens and streams the results of the specified executables to the
 * socket.
 *
 * If the path cannot be popened, then handle error with
 * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 **/
Status  handle_cgi_request(Request *r) {
    FILE *pfs;
    char buffer[BUFSIZ];

    /* Export CGI environment variables from request:
     * http://en.wikipedia.org/wiki/Common_Gateway_Interface */
    setenv("QUERY_STRING", r->query, 1);
    setenv("REMOTE_ADDR", r->host, 1);
    setenv("REMOTE_PORT", r->port, 1);
    setenv("REQUEST_METHOD", r->method, 1);
    setenv("REQUEST_URI", r->uri, 1);
    setenv("DOCUMENT_ROOT", RootPath, 1);
    setenv("SERVER_PORT", Port, 1);
    setenv("SCRIPT_FILENAME", r->path, 1);

    /* Export CGI environment variables from request headers */
    for (Header *h=r->headers; h; h = h->next) {
        if (streq(h->name, "Host")) {
            setenv("HTTP_HOST", h->data, 1);
        } else if (streq(h->name, "Accept")) {
            setenv("HTTP_ACCEPT", h->data, 1);
        } else if (streq(h->name, "Accept-Language")) {
            setenv("HTTP_ACCEPT_LANGUAGE", h->data, 1);
        } else if (streq(h->name, "Accept-Encoding")) {
            setenv("HTTP_ACCEPT_ENCODING", h->data, 1);
        } else if (streq(h->name, "User-Agent")) {
            setenv("HTTP_USER_AGENT", h->data, 1);
        } else if (streq(h->name, "Content-Type")) {
            setenv("HTTP_CONTENT_TYPE", h->data, 1);
        } else if (streq(h->name, "Content-Length")) {
            setenv("HTTP_CONTENT_LENGTH", h->data, 1);
        } else if (streq(h->name, "Accept-Language")) {
            setenv("HTTP_ACCEPT_LANGUAGE", h->data, 1);
        } else if (streq(h->name, "Cookie")) {
            setenv("HTTP_COOKIE", h->data, 1);
        }
    }

    /* POpen CGI Script */
    pfs = popen(r->path, "r");
    if (!pfs) {
        debug("Failed popen: %s", strerror(errno));
        return HTTP_STATUS_INTERNAL_SERVER_ERROR;
    }

    /* Copy data from popen to socket */
    while (fgets(buffer, BUFSIZ, pfs)) {
        fputs(buffer, r->stream);
    }

    /* Close popen, return OK */
    pclose(pfs);
    return HTTP_STATUS_OK;
}

/**
 * Handle displaying error page
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP error request.
 *
 * This writes an HTTP status error code and then generates an HTML message to
 * notify the user of the error.
 **/
Status  handle_error(Request *r, Status status) {
    const char *status_string = http_status_string(status);

    log("HTTP REQUEST STATUS: %s", status_string);

    /* Write HTTP Header */
    fprintf(r->stream, "HTTP/1.0 %s\r\n", status_string);
    fprintf(r->stream, "Content-Type: text/html\r\n");
    fprintf(r->stream, "\r\n");

    /* Write HTML Description of Error*/
    fprintf(r->stream, "<html>\n");
    fprintf(r->stream, "\t<h1>%s</h1>\n", status_string);
    fprintf(r->stream, "\t<p>There was an error processing your request.</p>\n");
    fprintf(r->stream, "</html>\n");

    /* Return specified status */
    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
